/**
 * @file main.c
 * @author Dekonega
 * @author Marakate
 * @date 08.02.2017
 * @brief File containing the main loop of Daikatana AutoUpdater.
 * @details Daikatana Auto-Updater. Based on MD5.EXE by John Walker: http://www.fourmilab.ch/
 * @copyright This program is in the public domain.
 */

#include <ctype.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <assert.h>

#ifdef __linux__
#include <getopt.h>
#include <libintl.h>
#include <stdbool.h>
#include <curses.h>
#include <linux/string.h>
#include <linux/types.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <libelf.h>
#include <elf.h>
#include <syslog.h>
#endif

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <io.h>
#include <windows.h>
#include <conio.h>
#endif

#include "include/md5.h"
#include "include/http_dl.h"
#include "include/shared.h"
#include "include/essentials.h"
#include "include/filesystem.h"

/**
 * Definitions.
 */
#define _(String) String
#define N_(String) String
#define textdomain(Domain)
#define bindtextdomain(Package, Directory)
#define true 1
#define false 0
#define VERSION "0.2"
#define program_name "DKAutoUpdate"
#define AUTHORS proper_name ("Daikatana Team and Dekonega")
#define emit_try_help() \
    do \
{ \
    fprintf (stderr, _("Try '%s --help' for more information.\n"), \
            program_name); \
} \
while (0)

#define HELP_OPTION_DESCRIPTION \
    _("    --help        display this help and exit\n")

#define VERSION_OPTION_DESCRIPTION \
    _("    --version     output version information and exit\n")


/**
 * Globals and other declarations.
 */

bool Debug = false;
// bool showfile = false;
char *hexfmt = (char*)"%02x";
static struct configuration_base conf;
static char msgString[100];
static unsigned char buffer[16];
static bool yes_to_all = false;
static int verbose_flag;
static struct option long_options[] = {
	//{"verbose", no_argument, &verbose_flag, 1},
	{"yes", no_argument, NULL, 'y'},
	{"debug", no_argument, NULL, 'd'},
	{"showfile", no_argument, NULL, 's'},
	{"help", no_argument, NULL, 'h'},
	{"version", no_argument, NULL, 'v'},
	{NULL, 0, NULL, 0}
};

struct configuration_base {
	bool enable_debug;
	bool display_file;
	bool enable_fake_network;
};

typedef struct {
	const bool dataFile;
	const char *fileName;
	char *md5FileName;
	char downloadfile[MAX_QPATH];
	char *filepath;
	unsigned char pakFileSignature[16];
	char pakHttp_md5[HTTP_SIG_SIZE];
	const char *description;
} pakfiles_t;

/**
 * @note Keep this first-line intact because the latest build should
 * have the latest pak3.pak.
 *
 * @note Linux version needs another kind of solution for
 * checking the binary.
 */
pakfiles_t pakfiles[] = {
#ifdef _WIN32
	{false, "daikatana.exe",   "dk_"__PLATFORM_EXT__".md5",    "",     "daikatana.exe", "",  "", "Daikatana Windows executable"},
#else
	{false, "daikatana",       "dk_"__PLATFORM_EXT__".md5",    "",     "daikatana",    "",  "", "Daikatana Linux executable"},
#endif
	{true, "pak4.pak",        "pak4.md5",             "pak4.pak",     "data/pak4.pak", "", "", "Widescreen HUD, Script Fixes, etc."},
	{true, "pak5.pak",        "pak5.md5",             "pak5.zip",     "data/pak5.pak", "", "", "32-bit Textures (Optional)"},
	{true, "pak6.pak",        "pak6.md5",             "pak6.zip",     "data/pak6.pak", "", "", "Map Updates (Recommended)"},
	{0}
};

/**
 * @brief Function prototypes.
 * @todo Make another file for all this junk and refractor the project one of
 * these days for better readability.
 */
void Error_Shutdown (void);
static bool DKAuto_Calc_MD5_File (pakfiles_t *pakfile);
static bool DKAuto_Check_MD5_Signatures (pakfiles_t *pakfile);
static bool DKAuto_CheckLinuxBinary (void);
static int DKAuto_Download_Loop (void);
static void DKAuto_Get_HTTP_Binary_Link (pakfiles_t  *pakfile);
static void DKAuto_Get_HTTP_MD5 (pakfiles_t *pakfile);
static char* DKAuto_LogTime (void);
static void DKAuto_ShutdownUpdater (void);
static void DKAuto_PrintUsage(int);

/**
 * @brief Inspect a file and determine if it's an ELF-executable.
 *
 * @param FILE pointer to a file we want to inspect.
 * @return true if file is executable, false if not.
 */
static bool DKAuto_CheckLinuxBinary(void)
{
	int fd = 0;
	Elf* dkelf = NULL;
	// char filename[20];
	FILE *fp = NULL;
	bool x86exec = false;
	Elf32_Ehdr *hdr = NULL;

	/** 1. Open file */
	if ((fd = open("daikatana", O_RDONLY)) < 0) {
		fprintf(stderr, "Error: Couldn't find daikatana executable!\n");
		exit(EXIT_FAILURE);
	}

	/**
	 * 2. Read first four bytes and match them to {0x7f, 'E', 'L', 'F'}.
	 * This can happen using the built-in functions for reading the header
	 * so don't worry about manually parsing the shit. Check that the libelf
	 * has all the stuff we need.
	 */
	if (elf_version(EV_CURRENT) == EV_NONE) {
		/* Update your distribution. */
	}

	/**
	 * The function returns a pointer to a ELF descriptor if successful,
	 * or NULL if an error occurred.
	 */
	if ((dkelf = elf_begin(fd, ELF_C_READ, (Elf *) 0)) == 0) {
		fprintf(stderr, "Error: Not an ELF-file!\n");
		exit(EXIT_FAILURE);
	}

	assert(dkelf);

	/**
	 * Return a pointer to a translated header descriptor if successful,
	 * or NULL on failure.
	 */
	if ((hdr = elf32_getehdr(dkelf)) == NULL) {
		fprintf(stderr, "Error: Wait a sec are u trying to cheat me again?\n");
		exit(EXIT_FAILURE);
	}

	assert(hdr);

	/** 3. Test if file is an actual ELF of either x86 or x86_64 type. */
	if (hdr != NULL) {
		switch (hdr->e_type) {
		case 1:
			printf("Detected ET_REL file!\n");
			break;
		case 2:
			printf("[%s] Detected ET_EXEC file!\n", DKAuto_LogTime());
			break;
		default:
			printf("Detected unknown file!\n");
		}

		switch (hdr->e_machine) {
		case 0:
			printf("Detected EM_NONE!\n");
			break;
		case 3:
			printf("[%s] Detected EM_386!\n", DKAuto_LogTime());
			break;
		case 62:
			printf("Detected EM_X86_64!\n");
			break;
		default:
			printf("Detected unknown!\n");
		}
	} else {
		fprintf(stderr, "Error: Header pointer points to NULL!\n");
		exit(EXIT_FAILURE);
	}

	/** 4. If they match ELF, return true */
	/** 5. If not, return false. */
	if (hdr->e_type == 2 && hdr->e_machine == 3) {
		x86exec = true;
	}

	return x86exec;
}

/**
 * @brief Output information about program usage.
 *
 * @param int status
 * @return char* String with time and message applied.
 * @note This was mostly just for fun. I could use the built-in clock in
 * printf just as well and without having to do this. I need to clean
 * this up.
 */
char* DKAuto_LogTime(void)
{
	time_t t;
	struct tm *bdtime;
	t = time(NULL);
	bdtime = localtime(&t);

	if (bdtime == NULL) {
		perror("localtime");
		exit(EXIT_FAILURE);
	}

	if (strftime(msgString, sizeof msgString, "%Y-%m-%d %H:%M:%S", bdtime)) {
		/* Put the time out into the stream. */
		// puts(msgString);

	} else {
		fprintf(stderr, "Error: strftime failed");
		exit(EXIT_FAILURE);
	}

	/**
	 * @todo I need to fix this bullshit so that when I just do something
	 * like log("logged message") that gets out without any additional
	 * bullshit. I need hangLog(char *) type stuff and a proper message queue.
	 */
	return msgString;
}

/**
 * @brief Output information about program usage.
 *
 * @param int status
 */
void DKAuto_PrintUsage (int status)
{
	if (status != EXIT_SUCCESS) {
		emit_try_help();
	} else {
		printf (_("\
Usage: %s [OPTION]...\n\
"), program_name);
		fputs(_("\
Automatic updater for Daikatana 1.3 GNU/Linux written in C.\n\
"), stdout);
		fputs (_("\
					\n\
-d, --debug       output diagnostic information.\n\
-s, --showfile    show more information about MD5 comparisons.\n\
-y, --yes         answer 'yes' to all questions.\n\
"), stdout);

		fputs (HELP_OPTION_DESCRIPTION, stdout);
		fputs (VERSION_OPTION_DESCRIPTION, stdout);
	}

	exit (status);
}

/**
 * @brief Calculate MD5 checksums from files.
 *
 * @param struct pakfiles_t
 * @return
 */
bool DKAuto_Calc_MD5_File (pakfiles_t *pakfile)
{
	int j;
	FILE *in = stdin, *out = stdout;
	unsigned char buffer[16384], signature[16];
	struct MD5Context md5c;
	bool opened = false;

	if ((in = fopen(pakfile->filepath, "rb")) == NULL) {
		// fprintf(stderr, "Cannot open input file %s\n", pakfile->filepath);
		pakfile->pakFileSignature[0] = '\0';

		return false;
	}
	opened = true;
#ifdef _WIN32

	/** @warning Warning! On systems which distinguish text mode and binary
	 * I/O (MS-DOS, Macintosh, etc.) the modes in the open statement for
	 * "in" should have forced the input file into binary mode.
	 * But what if we're reading from standard input?  Well,
	 * then we need to do a system-specific tweak to make sure it's in
	 * binary mode. While we're at it, let's set the mode to binary
	 * regardless of however fopen set it.
	 *
	 * The following code, conditional on _WIN32, sets binary mode using
	 * the method prescribed by Microsoft Visual C 7.0 ("Monkey C"); this
	 * may require modification if you're using a different compiler or
	 * release of Monkey C. If you're porting this code to a different
	 * system which distinguishes text and binary files, you'll need to
	 * add the equivalent call for that system.
	 */

	_setmode(_fileno(in), _O_BINARY);
#endif

	MD5Init(&md5c);

	while ((j = (int) fread(buffer, 1, sizeof buffer, in)) > 0) {
		MD5Update(&md5c, buffer, (unsigned) j);
	}

	if (opened) {
		fclose(in);
	}

	MD5Final(signature, &md5c);

	for (j = 0; j < sizeof signature; j++) {
		pakfile->pakFileSignature[j] = signature[j];

		if (conf.display_file) {
			fprintf(out, hexfmt, signature[j]);
		}
	}

	if (conf.display_file) {
		fprintf(out, "  %s", (in == stdin) ? "-" : pakfile->filepath);
		fprintf(out, "\n");
	}

	return true;
}

/**
 * @brief
 *
 * @param
 * @return
 */
void DKAuto_Get_HTTP_MD5 (pakfiles_t *pakfile)
{
	char url[MAX_URLLENGTH];
	int err = 0;

	memset((char *)pakfile->pakHttp_md5, 0, HTTP_SIG_SIZE);
	Com_sprintf(url, sizeof(url), "http://dk.toastednet.org/DK13/%s", pakfile->md5FileName);
	CURL_HTTP_StartMD5Checksum_Download (url, pakfile->pakHttp_md5);
	err = DKAuto_Download_Loop();

	if (err == HTTP_MD5_DL_FAILED) {
		memset((char *)pakfile->pakHttp_md5, 0, sizeof(pakfile->pakHttp_md5));
	} else {
		pakfile->pakHttp_md5[32] = '\0';
		DKAuto_Get_HTTP_Binary_Link(pakfile);
	}
}

/**
 * @brief
 *
 * @param
 * @return
 */
void DKAuto_Get_HTTP_Binary_Link(pakfiles_t *pakfile)
{
	char url[MAX_URLLENGTH];
	char fixedDownloadName[MAX_OSPATH];
	int err = 0;

	if (pakfile->dataFile == true) {

		return;
	}

	/** Example: http://dk.toastednet.org/DK13/dk_linux.txt */
	COM_StripExtension(pakfile->md5FileName, fixedDownloadName);
	Com_sprintf(url, sizeof(url), "http://dk.toastednet.org/DK13/%s.txt", fixedDownloadName);
	CURL_HTTP_StartMD5Checksum_Download (url, pakfile->downloadfile);
	err = DKAuto_Download_Loop();

	if (err == HTTP_MD5_DL_FAILED) {
		memset((char *)pakfile->downloadfile, 0, sizeof(pakfile->downloadfile));
	} else {
		for (unsigned long x = 0; x < sizeof(pakfile->downloadfile); x++) {
			/**
			 * @note FS: Echo in Windows command prompt adds a space and newline and
			 * fudges this up
			 */
			if ((pakfile->downloadfile[x] == ' ') || (pakfile->downloadfile[x] == '\n')) {
				pakfile->downloadfile[x] = '\0';
				break;
			}
		}
	}
}

/**
 * @brief Function that checks if MD5-signature of a PAK-file is a match or not.
 * @param pakfiles_t structure.
 * @return True if MD5-signature is a match, False if not.
 */
bool DKAuto_Check_MD5_Signatures (pakfiles_t *pakfile)
{
	int i;
	char convertedSignature[33];

	for (i = 0; i < 16; i++) {
		sprintf(convertedSignature + i * 2, "%02x", pakfile->pakFileSignature[i]);
	}

	if (pakfile->pakHttp_md5[0] == '\0') {
		printf("[%s] No HTTP Signature!\n", DKAuto_LogTime());

		return true;
	} else {
		if (!strcmp(convertedSignature, pakfile->pakHttp_md5)) {
			printf("[%s] %s is up to date.\n", DKAuto_LogTime(), pakfile->fileName);
			syslog(LOG_NOTICE, "%s was found to be up to date.", pakfile->fileName);

			return true;
		} else {
			printf("[%s] File mismatch!\n", DKAuto_LogTime());

			return false;
		}
	}

	return false;
}

/**
 * @brief
 *
 * @param
 * @return
 */
int DKAuto_Download_Loop (void)
{
	int bIsDone = 0;

	while (!bIsDone) {
		bIsDone = CURL_HTTP_Update();
		/**
		 * @todo Why are we having a handbreak here that can essentially
		 * kill the whole download process instantly or at least cause
		 * substantial slowing down?
		 */
		Sys_SleepMilliseconds(1);
	}

	return bIsDone;
}

/**
 * @brief
 * @param struct pakfiles_t
 * @param bool binary
 * @return
 */
void DKAuto_Get_PAK (pakfiles_t *pakfile, bool binary)
{
	int ch, answer;
	char url[MAX_URLLENGTH];
	char fileName[MAX_QPATH];

	if (pakfile->description && pakfile->description[0] != '\0') {
		printf("[%s] File Description: %s\n", DKAuto_LogTime(), pakfile->description);
	}

	printf("[%s] Would you like to download: %s? (y/n):", DKAuto_LogTime(), pakfile->downloadfile);

	while (1) {
		/** UGLY AS FUCK! ...but it works. */
		if (yes_to_all) {
			printf("\n");
			/* Magic Number. :^) */
			answer = 121;
		} else {
			/**
			 * Read the first character of the line.
			 * This should be the answer character, but might not be.
			 */
			ch = tolower (fgetc (stdin));
			answer = ch;

			/** Discard rest of input line. */
			while (ch != '\n' && ch != EOF) {
				ch = fgetc (stdin);
			}
		}

		/** Obey the answer if it was valid. */
		if (answer == 121) {
			printf("[%s] Will start downloading file.\n", DKAuto_LogTime());

			if (!binary) {
				/** Create a folder "data" where files will go. */
				Sys_Mkdir((char*)"data");
			}
			/**
			 * @note Undefined behaviour might happen. And I want to drop
			 * the weird Q2 Com_sprintf from the code. But it has
			 * the size check so it's useful.
			 */
			Com_sprintf(url, sizeof(url), "http://dk.toastednet.org/DK13/%s", pakfile->downloadfile);
			/**
			 * @note Example of a formed URL:
			 * http://dk.toastednet.org/DK13/Daikatana-Linux-2017-03-12.tar.bz2
			 */
			if (!binary) {
				Com_sprintf(fileName, sizeof(fileName), "data/%s", pakfile->downloadfile);

			} else {
				Com_sprintf(fileName, sizeof(fileName), "%s", pakfile->downloadfile);
			}

			/** Ask curl to download a file. */
			CURL_HTTP_StartDownload(url, fileName);

			/** So something? Visualise download? */
			DKAuto_Download_Loop();

			break;
		}
		if (answer == 'n') {
			printf("[%s] Skipping to next file...\n", DKAuto_LogTime());

			break;
		}
		/**
		 * Answer was invalid: Ask again for a valid answer.
		 */
		fputs ("Please answer y or n: ", stdout);
	}
}

/**
 * @brief
 *
 * @param
 * @return
 */
void DKAuto_Check_MD5_vs_Local (pakfiles_t *pakfile)
{
	printf("[%s] Checking %s\n", DKAuto_LogTime(), pakfile->fileName);

	/**
	 * What is love? Baby don't hurt me.
	 */
	if (pakfile->pakFileSignature[0] == 0) {
		printf("[%s] File not found!\n", DKAuto_LogTime());

		/* @note FS: Compare filename instead of downloaded file
		 * because it won't be set yet if the file is missing.
		 *
		 * @note This ".exe" is going to be broken on Linux before the
		 * Elf checks are properly working.
		 */
		if (pakfile->dataFile == true) {
			DKAuto_Get_PAK(pakfile, false);
		} else {
			fprintf(stderr, "Error: Please run DKAutoUpdate from your Daikatana root directory!\n");
			Error_Shutdown();
		}
	} else {
		bool bSameFile = false;
		DKAuto_Get_HTTP_MD5(pakfile);
		bSameFile = DKAuto_Check_MD5_Signatures(pakfile);

		if (!bSameFile) {
			if (pakfile->dataFile == true) {
				DKAuto_Get_PAK(pakfile, false);
			} else {
				DKAuto_Get_PAK(pakfile, true);
			}
		}
	}
}

/**
 * @brief Cleanly shutdown AutoUpdater.
 */
void DKAuto_ShutdownUpdater(void)
{
	CURL_HTTP_Shutdown();
	NET_Shutdown();
}

/**
 * @brief In case of an error, attempt to gracefully shutdown the AutoUpdater.
 */
void Error_Shutdown(void)
{
	DKAuto_ShutdownUpdater();
	exit(EXIT_FAILURE);
}

/**
 * @brief Main procedure of DKAutoUpdate.
 */
int main(int argc, char **argv)
{
	/** @note optc status code operator for dealing with the parameters. */
	int c = 0;
	int option_index = 0;
	int x = 0;

	/** @todo I want to parse the given parameters using GNU getops. */
	while ((c = getopt_long (argc, argv, "ydshv", long_options, &option_index)) != -1) {
		switch (c) {
		case'y':
			puts("Answer yes to all questions.\n");
			yes_to_all = true;
			break;
		case 'd':
			// puts ("Debug mode flag.\n");
			conf.enable_debug = true;
			Debug = true;
			break;
		case 's':
			// puts ("Display files flag.\n");
			conf.display_file = true;
			break;
		case 'h':
			DKAuto_PrintUsage (EXIT_SUCCESS);
			break;
		case 'v':
			printf("Version %s, Compiled: %s, %s\n", VERSION, __DATE__, __TIME__);
			return (EXIT_SUCCESS);
			break;
		default:
			DKAuto_PrintUsage (EXIT_FAILURE);
		}
	}

	/** What would be the difference between verbose and debug modes? */
	if (verbose_flag) {
		// puts ("Verbose mode set.\n");
	}

	/** Call function from an external library. */
	if (conf.enable_debug) {
		printf("[%s] Debug mode set.\n", DKAuto_LogTime());
	}

	// return 0; // copout.

#ifdef CHECK_HARDWARE_PROPERTIES
	/**
	 * @warning Verify unit32 is, in fact, a 32 bit data type.
	 */
	if (sizeof(uint32) != 4) {
		fprintf(stderr, "** Configuration error.  Setting for uint32 in file md5.h\n");
		fprintf(stderr, "   is incorrect.  This must be a 32 bit data type, but it\n");
		fprintf(stderr, "   is configured as a %d bit data type.\n", ((int) sizeof(uint32) * 8));
		return 2;
	}

	/**
	 * @warning If HIGHFIRST is not defined, verify that this machine is,
	 * in fact, a little-endian architecture.
	 */
#ifndef HIGHFIRST
	{
		uint32 t = 0x12345678;

		if (*((char *) &t) != 0x78) {
			fprintf(stderr, "** Configuration error.  Setting for HIGHFIRST in file md5.h\n");
			fprintf(stderr, "   is incorrect.  This symbol has not been defined, yet this\n");
			fprintf(stderr, "   machine is a big-endian (most significant byte first in\n");
			fprintf(stderr, "   memory) architecture.  Please modify md5.h so HIGHFIRST is\n");
			fprintf(stderr, "   defined when building for this machine.\n");
			return 2;
		}
	}
#endif
#endif
	/** Initialise the network and other stuff. */
	NET_Init();
	CURL_HTTP_Init();
	syslog(LOG_NOTICE, "Checking Updates for Daikatana.");

	/** Process command line options and show banner.*/
	printf("---------------------------------------------------------------\n");
	printf("\nDaikatana v1.3 Auto-Updater for %s. Version %s\n\n", PLATFORM, VERSION);
	printf("---------------------------------------------------------------\n");

#ifdef __linux__

	printf("[%s] Platform Extension: %s\n", DKAuto_LogTime(), __PLATFORM_EXT__);

	/**
	 * Perform absolute minimum checks for the Daikatana binary before
	 * doing anything else.
	 */
	if (!DKAuto_CheckLinuxBinary()) {
		fprintf(stderr, "Not a Daikatana binary?!\n");
		exit(EXIT_FAILURE);
	}
#endif
	/** @note Walk through the pakfiles struct, check what we got. */
	while (pakfiles[x].fileName != NULL) {
		/** Calculate MD5-checksums. */
		DKAuto_Calc_MD5_File (&pakfiles[x]);
		/** Compare MD5-checksums. */
		DKAuto_Check_MD5_vs_Local(&pakfiles[x]);
		x++;
	}

	DKAuto_ShutdownUpdater();

	return (EXIT_SUCCESS);
}
