/**
 * @file http_dl.c
 * @author Dekonega
 * @author Marakate
 * @date 28.03.2017
 * @brief Download code utilising libCURL.
 * @details Part of the Daikatana Auto-Updater.
 * @copyright This program is in the public domain.
 */

#include <limits.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/** Additional system libraries. */
#include <sys/io.h>
#include <curl/curl.h>
#include <unistd.h>

/** Needed for unpacking .tar.bz2, should be refactored. */
#include <archive.h>
#include <archive_entry.h>

#ifdef _WIN32
#include <io.h>
#include <conio.h>
#endif

#include "http_dl.h"
#include "essentials.h"
#include "filesystem.h"
#include "dg_misc.h"

#define CURL_STATICLIB
#define CURL_DISABLE_LDAP
#define CURL_DISABLE_LDAPS
#define CURL_ERROR(x)   curl_easy_strerror(x)
#define STOP_DOWNLOAD_AFTER_THIS_MANY_BYTES         6000
#define MINIMAL_PROGRESS_FUNCTIONALITY_INTERVAL     3

static int curl_borked;
static CURL *easy_handle;
static CURLM *multi_handle;
static curl_dltype_t dltype;

static FILE *download;

#ifdef _WIN32
static char name[MAX_PATH];
#endif

static char name[PATH_MAX];
static char completedURL[MAX_URLLENGTH];

/** @note FS: For KBps calculator */
static int downloadpercent = 0;
static double prevSize = 0;

/** @note FS: For KBps */
static int previousTime = 0;
static float bytesRead = 0;
static float byteCount = 0;
static float timeCount = 0.0;
static float previousTimeCount = 0.0;
static float startTime = 0.0;
static float downloadrate = 0.0;

/**
 * @brief Required for reaing the copy operation
 * @note Taken from the libarchive website.
 */
int copy_data(struct archive *ar, struct archive *aw)
{
	int r;
	const void *buff;
	size_t size;
	la_int64_t offset;

	for (;;) {
		r = archive_read_data_block(ar, &buff, &size, &offset);

		if (r == ARCHIVE_EOF) {
			return (ARCHIVE_OK);
		}

		if (r < ARCHIVE_OK) {
			return (r);
		}

		r = archive_write_data_block(aw, buff, size, offset);

		if (r < ARCHIVE_OK) {
			fprintf(stderr, "%s\n", archive_error_string(aw));
			return (r);
		}
	}
}

/**
 * @brief Lets test out libarchive.
 * @note Taken from libarchive website.
 */
void extract(const char *filename)
{
	struct archive *a;
	struct archive *ext;
	struct archive_entry *entry;
	int flags;
	int r;

	/* Select which attributes we want to restore. */
	flags = ARCHIVE_EXTRACT_TIME;
	flags |= ARCHIVE_EXTRACT_PERM;
	flags |= ARCHIVE_EXTRACT_ACL;
	flags |= ARCHIVE_EXTRACT_FFLAGS;

	a = archive_read_new();
	archive_read_support_filter_all(a);
	archive_read_support_format_all(a);

	ext = archive_write_disk_new();
	archive_write_disk_set_options(ext, flags);
	archive_write_disk_set_standard_lookup(ext);

	if ((r = archive_read_open_filename(a, filename, 10240))) {
		fprintf(stderr, "Error: Unable to open the archive!\n");
		exit(EXIT_FAILURE);
	}

	for (;;) {
		r = archive_read_next_header(a, &entry);

		if (r == ARCHIVE_EOF) {
			break;
		}

		if (r < ARCHIVE_OK) {
			fprintf(stderr, "%s\n", archive_error_string(a));
		}

		if (r < ARCHIVE_WARN) {
			exit(EXIT_FAILURE);
		}

		r = archive_write_header(ext, entry);

		if (r < ARCHIVE_OK) {
			fprintf(stderr, "%s\n", archive_error_string(ext));

		} else if (archive_entry_size(entry) > 0) {
			r = copy_data(a, ext);

			if (r < ARCHIVE_OK) {
				fprintf(stderr, "%s\n", archive_error_string(ext));
			}

			if (r < ARCHIVE_WARN) {
				exit(EXIT_FAILURE);
			}
		}

		r = archive_write_finish_entry(ext);

		if (r < ARCHIVE_OK) {
			fprintf(stderr, "%s\n", archive_error_string(ext));
		}

		if (r < ARCHIVE_WARN) {
			exit(EXIT_FAILURE);
		}
	}

	archive_read_close(a);
	archive_read_free(a);
	archive_write_close(ext);
	archive_write_free(ext);
}

/**
 * @brief Visualise and show information about the download process.
 */
void CURL_Download_Calculate_KBps (double byteDistance, double totalSize)
{
	// fflush(stdout);

	/** byteDistance: 1448??? */
	// printf("\nbyteDistance: %f, totalSize: %f\n", (float)byteDistance, (float)totalSize);
	// printf("Sys_Milliseconds(): %d\n", Sys_Milliseconds());

	// startTime = (float)Sys_Milliseconds();
	// printf("startTime: %f\n", startTime);

	/**
	 * @todo There is a bug here somewhere that messes up the download time
	 * calculations on Linux. Probaby non-portable Sys_Milliseconds is the
	 * culprit here.
	 */

	// float timeDistance = 0;
	// float totalTime = 0;

	float timeDistance = (float)(Sys_Milliseconds() - previousTime);
	// printf("\ntimeDistance: %f\n", timeDistance);

	float totalTime = (timeCount - startTime) / 1000.0f;
	// printf("totalTime: %f\n", totalTime);

	timeCount += (float)timeDistance;
	// printf("timeCount: %f\n", timeCount);

	byteCount += (float)byteDistance;
	// printf("byteCount: %f\n", byteCount);

	bytesRead += (float)byteDistance;
	// printf("bytesRead: %f\n", bytesRead);

	/**
	 * @note We have to flush the buffered stream with \r since it doesn't
	 * do it by itself and fucks up the terminal.
	 */
	fflush(stdout);

	/**
	 * @note Show this piece of shit download meter regardless.
	 */
	printf(/*Rate: %7.2f KB/s,*/
	    "Downloaded %4.2f MB of %4.2f MB [%i%%]\r",
	    /** @todo downloadrate is fucked!
	    downloadrate, */
	    (float)(bytesRead / 1024.0f) / 1024.0f,
	    (float)(totalSize / 1024.0f) / 1024.0f,
	    downloadpercent);

	/**
	 * This has deep issues I'm too tired to think now.
	 */
	if (totalTime >= 1.0f) {
		/* downloadrate is fucked! */
		downloadrate = (float)byteCount / 1024.0f;

		/** @note FS: Start over... */
		byteCount = 0;
		startTime = (float)Sys_Milliseconds();
	}

	previousTime = (float)Sys_Milliseconds();
}


/**
 * @brief
 */
void CURL_HTTP_Reset_KBps_Counter (void)
{
	previousTime = bytesRead = byteCount = 0;
	timeCount = previousTimeCount = 0.0f;
	startTime = (float)Sys_Milliseconds();
	prevSize = 0;
	downloadpercent = 0;
}

/**
 * @brief
 */
void CURL_HTTP_Calculate_KBps (double curSize, double totalSize)
{
	double byteDistance;

	byteDistance = curSize - prevSize;
	prevSize = curSize;
	CURL_Download_Calculate_KBps (byteDistance, totalSize);
}

/**
 * @brief
 */
static int http_progress (void *clientp, double dltotal, double dlnow,
                          double ultotal, double uplow)
{
	if (dltotal) {
		downloadpercent = (int)((dlnow / dltotal) * 100.0f);
		// printf("Download now: %f\n", dlnow);
		CURL_HTTP_Calculate_KBps(dlnow, dltotal);
	} else {
		downloadpercent = 0;
	}
	/** non-zero = abort */
	return 0;
}

/**
 * @brief
 */
static size_t http_write (void *ptr, size_t size, size_t nmemb, void *stream)
{
	return fwrite (ptr, 1, size * nmemb, download);
}

/**
 * @brief
 */
static size_t http_write_md5 (void *ptr, size_t size, size_t nmemb, void *stream)
{
	if (nmemb >= HTTP_SIG_SIZE) {
		fprintf(stderr, "Error: temporary file greater than buffer!\n");
		fprintf(stderr, "Please report this as a bug: %s!\n", completedURL);
		Error_Shutdown();
	}

	memcpy(stream, ptr, nmemb);

	return 0;
}

/**
 * @brief Init cURL for downloading.
 */
void CURL_HTTP_Init (void)
{
	if ((curl_borked = curl_global_init (CURL_GLOBAL_NOTHING))) {

		return;
	}

	multi_handle = curl_multi_init ();
}

/**
 * @brief Clean up after downloading files.
 */
void CURL_HTTP_Shutdown (void)
{
	if (curl_borked) {

		return;
	}

	curl_multi_cleanup (multi_handle);
	curl_global_cleanup ();
}

/**
 * @brief Download a file.
 */
void CURL_HTTP_StartDownload (const char *url, char *filename)
{
	/** Reset the counters before starting to download a new file. */
	CURL_HTTP_Reset_KBps_Counter();

	if (!filename || filename[0] == '\0') {
		fprintf(stderr, "Error: %s: Filename is blank!\n", __func__);
		Error_Shutdown();

		return;
	}

	if (!url || url[0] == '\0') {
		fprintf(stderr, "Error: %s: URL is blank!\n", __func__);
		Error_Shutdown();

		return;
	}

	Com_sprintf(name, sizeof(name), "%s", filename);

	if (!download) {
		download = fopen (name, "wb");

		if (!download) {
			fprintf (stderr, "Error: %s: Failed to open %s\n", __func__, name);
			Error_Shutdown();

			return;
		}
	}

	if (strstr(filename, ".zip")) {

		dltype = zipfile;
	} else {

		dltype = file;
	}

	Com_sprintf(completedURL, sizeof(completedURL), "%s", url);
	Con_DPrintf("[I] HTTP File Download URL: %s\n", completedURL);
	easy_handle = curl_easy_init();
	curl_easy_setopt (easy_handle, CURLOPT_NOPROGRESS, 0L);
	curl_easy_setopt (easy_handle, CURLOPT_NOSIGNAL, 1L);
	curl_easy_setopt (easy_handle, CURLOPT_WRITEFUNCTION, http_write);
	curl_easy_setopt (easy_handle, CURLOPT_URL, completedURL);
	/**
	 * @note CURLOPT_PROGRESSFUNCTION is possibly obsolete in current Linux
	 * cURL libraries?
	 */
	curl_easy_setopt (easy_handle, CURLOPT_PROGRESSFUNCTION, http_progress);
	/**
	 * @note This is how it currently works but isn't a "drag n' drop
	 * replacement for the older functionality.
	 */
	// curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, xferinfo);
	curl_multi_add_handle (multi_handle, easy_handle);
}

/**
 * @brief Download a checksum file.
 */
void CURL_HTTP_StartMD5Checksum_Download (const char *url, void *stream)
{
	CURL_HTTP_Reset_KBps_Counter();

	if (!stream) {
		fprintf(stderr, "Error: %s: stream is NULL!\n", __func__);

		return;
	}

	if (!url || url[0] == '\0') {
		fprintf(stderr, "Error: %s: URL is NULL!\n", __func__);

		return;
	}

	dltype = md5;
	Com_sprintf(completedURL, sizeof(completedURL), "%s", url);
	Con_DPrintf("[I] HTTP Checksum Download URL: %s\n", completedURL);
	easy_handle = curl_easy_init ();
	curl_easy_setopt (easy_handle, CURLOPT_NOPROGRESS, 1L);
	curl_easy_setopt (easy_handle, CURLOPT_NOSIGNAL, 1L);
	curl_easy_setopt (easy_handle, CURLOPT_WRITEDATA, stream);
	curl_easy_setopt (easy_handle, CURLOPT_WRITEFUNCTION, http_write_md5);
	curl_easy_setopt (easy_handle, CURLOPT_URL, completedURL);
	curl_multi_add_handle (multi_handle, easy_handle);
}

/**
 * @brief Function to handle what happens to downloaded files.
 */
void CURL_HTTP_Handle_Files()
{
	char* directory;
	char tarcommand[128];

	if (downloadpercent) {
		fflush(stdout);
		printf(/*Rate: %7.2f KB/s,*/
		    "Downloaded %4.2f MB of %4.2f MB [%i%%]\r",
		    /** @todo downloadrate is fucked!
		    downloadrate,*/
		    (float)(bytesRead / 1024.0f) / 1024.0f,
		    (float)(bytesRead / 1024.0f) / 1024.0f,
		    downloadpercent);
		// printf("Download complete!\n");
	}

	if (dltype >= file) {
		/** @note FS: Tell user when it's done. */
		printf ("HTTP Download of %s completed\n", name);
	}

	if (download) {
		fclose(download);
	}

	if (dltype == zipfile) {
		/**
		 * @todo There is something funny going on
		 * here and it needs to be investigated.
		 */
		char dlFileInZip[MAX_OSPATH];
		char dlPath[MAX_OSPATH];

		Com_sprintf(dlFileInZip, sizeof(dlFileInZip), "%s", name);
		COM_StripExtension(dlFileInZip, dlFileInZip);
		strcat(dlFileInZip, ".pak");

		for (int i = 5; i < sizeof(dlFileInZip); i++) {
			/** @note FS: Skip past "data/" directory. */
			if (dlFileInZip[i] == '\0') {
				dlPath[i - 5] = '\0';
				break;
			}
			dlPath[i - 5] = dlFileInZip[i];
		}

		FS_DecompressFile(dlFileInZip, name, dlPath);

		if ((Sys_DeleteFile(name)) < 0) {
			fprintf(stderr, "Error deleting temporary file: %s\n", name);
			Sys_Error();
		}
	}

	if (dltype >= file) {
		/**
		 * The strcasestr() function is like strstr(),
		 * but ignores the case of both arguments.
		 * The strcasestr() function is a nonstandard extension. :-(
		 *
		 * Maybe if I just change it lower case since it's essentially
		 * just a switch for triggering this condition?
		 */
		if (strstr(name, ".tar.bz2") || strstr(name, ".TAR.BZ2")) {
			printf("Starting Daikatana v1.3 Upgrade...\n");

			/**
			 * Will unpack the archive but the top level directory
			 * is such a headache at the moment for me that I'm
			 * going to copout and maybe in next version fix this.
			 */
			// extract(name);

			/**
			 * Form a command to unpack the stuff.
			 */
			strcpy(tarcommand, "tar --strip-components 1 -xvf ");
			strcat(tarcommand, name);
			fflush(stdin);
			system(tarcommand);
			printf("Unpacking complete!\n");

			// directory = name;
			// printf("%s\n", directory);
			// removeSubstr(directory, ".tar.bz2");
			// removeSubstr(directory, ".TAR.BZ2");
			// printf("%s\n", directory);
			// exit(EXIT_FAILURE);
			// move_update_to_folder(directory, "..")

			if ((Sys_DeleteFile(name)) < 0) {
				fprintf(stderr, "Error deleting temporary file: %s\n", name);
				Sys_Error();
			}
		}
	}
	/** Reset download? */
	download = NULL;
}

static void removeSubstr (char *string, char *sub)
{
	char *match;
	int len = strlen(sub);

	while ((match = strstr(string, sub))) {
		*match = '\0';
		strcat(string, match + len);
	}
}
/*
void move_update_to_folder(char* dst_directory, char* src_directory)
{
    DIR *dp;
    struct dirent *entry;
    struct stat statbuf;
    struct tm *tm;
    char src_folder[1024];
    char dest_folder[1024];

    if ((dp = opendir(src_directory)) == NULL) {
        fprintf(stderr, "cannot open directory: %s\n", src_directory);
        return;
    }

    chdir(src_directory);

    while ((entry = readdir(dp)) != NULL) {
        lstat(entry->d_name, &statbuf);
        if (!S_ISDIR(statbuf.st_mode)) {
            sprintf(src_folder, "%s%s", src_directory, entry->d_name);
            sprintf(dest_folder, "%s%s", dst_directory, entry->d_name);
            printf("%s----------------%s\n", entry->d_name, dest_folder);
            rename(src_folder, dest_folder);
        }
    }
    chdir("..");
    closedir(dp);
}
*/
int CURL_HTTP_Update (void)
{
	int         running_handles;
	int         messages_in_queue;
	long        response_code;
	CURLMsg     *msg;

	curl_multi_perform (multi_handle, &running_handles);

	while (msg = curl_multi_info_read (multi_handle, &messages_in_queue) ) {
		if (msg->msg == CURLMSG_DONE) {
			curl_easy_getinfo (msg->easy_handle, CURLINFO_RESPONSE_CODE, &response_code);
			Con_DPrintf("HTTP URL response code: %li\n", response_code);
			/**
			 * @todo There are still too many nested if-statements!
			 * Functions should be used instead.
			 */
			if (response_code == HTTP_OK || response_code == HTTP_REST) {
				CURL_HTTP_Handle_Files();

			} else {
				if (downloadpercent) {
					printf("\n");
				}

				if (download) {
					fclose(download);
					Sys_DeleteFile(name);
				}
				download = NULL;
				fprintf(stderr, "Error: HTTP Download Failed: %ld.\n", response_code);
				CURL_HTTP_Reset();

				return HTTP_MD5_DL_FAILED;

			}

			CURL_HTTP_Reset();
		}

		return 1;
	}

	return 0;
}

void CURL_HTTP_Reset (void)
{
	curl_multi_remove_handle (multi_handle, easy_handle);
	curl_easy_cleanup (easy_handle);
	easy_handle = 0;
	dltype = none;
}
