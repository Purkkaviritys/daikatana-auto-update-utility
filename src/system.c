#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <sys/time.h>

#ifdef WIN32
#include <windows.h>
#elif _POSIX_C_SOURCE >= 199309L
#include <time.h>
#else
#include <unistd.h>
#endif

#include "essentials.h"
#include "shared.h"

// unsigned long curtime = 0;
// WSADATA ws;

void NET_Init (void)
{
	/* We don't need to do initialization in Linux as it's done for us
	 * automatically... I think...
	 int err;

	 err = WSAStartup ((WORD)MAKEWORD (1,1), &ws);
	 if (err)
	 {
	 printf("Error loading Windows Sockets! Error: %i\n",err);
	 Error_Shutdown();
	 }
	 else
	 {
	 Con_DPrintf("[I] Winsock Initialized\n");
	 }
	 */
}

void NET_Shutdown (void)
{
	// WSACleanup();
}

void Sys_Mkdir (char *path)
{
	int status;

	status = mkdir(path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
}

int Sys_DeleteFile (const char *file)
{
	/* GNU remove() returns 0 on success, -1 when it fails and sets errno. */
	return remove(file);
}

void Sys_ClearConScreen (void)
{
	system("clear");
}

void Sys_Error (void)
{
	/** FS: Get the error message. Was an char array of 512. */
	int error_buff = 0;

	// FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), error_buff, sizeof(error_buff), NULL);
	/** @todo: Error message. */
	strerror( error_buff );
	fprintf (stderr, "Error: %d'\n", error_buff) ;
}

void Sys_SleepMilliseconds (int ms)
{
	if (ms < 0) {

		return;
	}

#ifdef WIN32
	/* @note I almost slit my wrist after seeing this Sleep function here
	 * since at that point I realised what had caused all my DL issues.
	 */
	Sleep(ms);
#elif _POSIX_C_SOURCE >= 199309L
	/* @note In Linux "sleep" is in SECONDS instead of MILLISECONDS like in
	 * Windows or something. That IS NOT the POSIX standard way and causes this
	 * shit to stuck constantly.
	 */
	struct timespec ts;
	ts.tv_sec = ms / 1000;
	ts.tv_nsec = (ms % 1000) * 1000000;
	nanosleep(&ts, NULL);
#else
	usleep(ms * 1000);
#endif

}

int timeGetTime()
{
	struct timeval now;
	/**
	* Returns time relative to an epoch. On Windows it's relative
	* to system startup time.
	*/
	gettimeofday(&now, NULL);

	return now.tv_usec / 1000;
}

int curtime;

int Sys_Milliseconds (void)
{
	/* @note Doom 3's source code has similar and POSIX comp func. */
	static unsigned long base = 0;
	static bool initialized = false;
	// struct timeval now;

	if (!initialized) {
		/**
		 * @note let base retain 16 bits of effectively random data. Note
		 * that the timeGetTime() only works like this on Windows.
		 */
		base = timeGetTime() & 0xffff0000;
		// base = now.tv_usec / 1000;
		// printf("base is now: %d\n", base);
		initialized = true;
	}

	// curtime = (now.tv_usec / 1000) - base;
	// printf("curtime is now: %d\n", curtime);
	// for (long x = 0; x < 1000000; ++x) {}
	curtime = timeGetTime() - base;
	// curtime = (now.tv_sec - base) * 1000 + now.tv_usec / 1000;
	// fflush(stdout);
	// printf("curtime 2 is now: %d\n", curtime);
	// exit(1);

	return curtime;

	/** Doom 3's Sys_Milliseconds */
	/*
	unsigned long sys_timeBase = 0;
	int curtime;
	struct timeval tp;

	gettimeofday(&tp, NULL);

	if (!sys_timeBase) {
	    sys_timeBase = tp.tv_sec;

	    return tp.tv_usec / 1000;
	}
	curtime = (tp.tv_sec - sys_timeBase) * 1000 + tp.tv_usec / 1000;

	return curtime;
	*/
}
