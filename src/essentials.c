#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/** @note FS: Caedes special string safe stuff */
#define DG_MISC_IMPLEMENTATION
#include "essentials.h"

/**
 * @brief Special sprintf implementation.
 * @note FS: From KMQ2.
 *
 */
void Com_sprintf(char* dest, int size, const char* fmt, ...)
{
	/** @note DG: implement this with vsnprintf() instead of a big buffer etc. */
	va_list argptr;

	va_start(argptr, fmt);
	KMQ2_vsnprintf(dest, size, fmt, argptr);
	/** @todo print warning if cut off! */
	va_end(argptr);
}

/**
 * @brief Print debug messages.
 * @note FS: Adapted From KMQ2.
 *
 */
void Con_DPrintf(const char* fmt, ...)
{
	va_list argptr;
	char msg[MAXPRINTMSG];

	/** @note Don't confuse non-developers with techie stuff. */
	if (!Debug) {

		return;
	}

	va_start(argptr, fmt);
	//  vsprintf (msg,fmt,argptr);

	/** @note Knightmare 10/28/12- buffer-safe version */
	KMQ2_vsnprintf(msg, sizeof(msg), fmt, argptr);
	va_end(argptr);
	printf("%s", msg);
}

/**
 * @brief Strip file extensions.
 * @note FS: From Q2.
 *
 */
void COM_StripExtension(char* in, char* out)
{
	while (*in && *in != '.') {
		*out++ = *in++;
	}
	*out = 0;
}
