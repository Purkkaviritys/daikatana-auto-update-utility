Daikatana Auto-Update for Linux
=====================================

Overview
--------
Basically it updates your Daikatana installation.

Building
--------
Good luck figuring out how to install the dependencies on your distribution.

Once you've installed all the things through your package manager or whatever you're ready do build the thing.

To build, do:

	rm -rf build
	mkdir build
	cd build
	cmake ..
	make
	strip DKAutoUpdate

Acknowledgements
----------------

Everybody in Daikatana community.